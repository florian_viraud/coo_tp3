from tp2 import *
def test_box_create():
    b = Box()

def test_box_add():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    assert "truc1" in b
    assert not  "bidule" in b


def test_box_remove():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    b.remove("truc1")
    assert "truc2" in b
    assert not "truc1" in b


def test_ouvert():
    b = Box()
    #par default doit etre fermer
    assert not b.is_open()
    b.open()
    assert b.is_open()
    b.close()
    assert not b.is_open()

def test_look():
    b = Box()
    assert b.action_look()=="La boite est fermee"
    b.add("truc")
    b.add("truc2")
    b.open()
    assert b.action_look()=="La boite contient : truc, truc2"


def test_objet_create():
    t = Thing(3,'a')


def test_volume():
    t = Thing(3,'a')
    t.volume()== 3

def test_capacite():
    b = Box()
    assert b.getCap()== None
    b.setcapacite(5)
    assert b.getCap()==5

def test_cap_None():
    b= Box()
    b.capacity()==None


def test_esapce():
    b=Box()
    t = Thing(300,'a')
    assert b.has_room_for(t)
    b.setcapacite(5)
    assert not b.has_room_for(t)
    b.setcapacite(500)
    assert b.has_room_for(t)

def test_action_add():
    b=Box()
    t = Thing(300,'a')
    b.open()
    assert b.action_add(t)
    assert t in b
    b.remove(t)
    b.setcapacite(20)
    assert not b.action_add(t)
    assert not t in b
    b.close()
    b.setcapacite(200000)
    assert not b.action_add(t)
    assert not t in b

def test_has_name():
    t = Thing(300,'a')
    assert t.has_name('a')
    assert not t.has_name('b')

def test_find():
    b=Box()
    t = Thing(300,'a')
    b.open()
    b.action_add(t)
    b.find('a')== t
    b.find('b')== None
    b.close()
    b.find('a')== None
    b.find('b')== None


def test_nv_createur():
    b=Box(400,True)
    t = Thing(300,'a')
    b.action_add(t)
    assert t in b #car avant initialiser fermer donc test l inverse
    k= Thing(8)
    k.getNom()==None
