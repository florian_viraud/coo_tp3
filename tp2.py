class Box:
    def __init__(self,capacite=None,ouvert=False):
        self._contenu = []
        self._ouvert = ouvert
        self._capacity = capacite

    def add(self,objet):
        self._contenu.append(objet)

    def remove(self,objet):
        self._contenu.remove(objet)

    def __contains__(self,machin):
        return machin in self._contenu

    def is_open(self):
        return self._ouvert

    def open(self):
        self._ouvert=True

    def close(self):
        self._ouvert=False

    def action_look(self):
        if self._ouvert:
            return "La boite contient : "+", ".join(self._contenu)
        return "La boite est fermee"
    def setcapacite(self,taille):
        self._capacity = taille

    def capacity(self):
        return self._capacity

    def getCap(self):
        return self._capacity

    def has_room_for(self,t):
        return self.getCap()==None or t.volume() < self.getCap()

    def action_add(self,t):
        if self.is_open():
            if self.has_room_for(t):
                self.add(t)
                if self.getCap()!=None:
                    self.setcapacite(self.getCap()-t.volume())
                return True
        return False

    def find(self,nom):
        if self.is_open():
            for objet in self._contenu:
                if objet.getNom()== nom:
                    return objet
        return None


class Thing:
    def __init__(self,taille,nom = None):
        self._taille=taille
        self._nom=nom

    def getNom(self):
        return self._nom

    def volume(self):
        return self._taille
    def has_name(self,nom):
        return self.getNom()==nom
